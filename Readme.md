# Public Python Scripts

Various Python scripts I wrote, free of use :slightly_smiling_face:

# Scripts list

## get_sun_elevation.py

Get max sun elevation angle for a given location.

Based on python [ephem](https://pypi.org/project/ephem) module.

Location can be :
* gps coordinate
* address : in that case, geopy [Nomatim](https://geopy.readthedocs.io/en/stable/#nominatim) geocoder is used

Example of use :

```bash
$ ./get_sun_elevation.py -a "1 place de la concorde 75008 Paris" -d 2022-06-01 -n 30 -v
# address found: Place de la Concorde, Quartier des Champs-Élysées, Paris 8e Arrondissement, Paris, Île-de-France, France métropolitaine, 75008, France
# gps coords: 48.8660227,2.3198494
2022-06-01 12:00:00: 63.14
2022-06-02 12:00:00: 63.27
[...]
2022-06-29 12:00:00: 64.33
2022-06-30 12:00:00: 64.27


$ ./get_sun_elevation.py -c 48.8660227,2.3198494 -d 2022-06-01
2022-06-01 12:00:00: 63.14
```

## get_gps_coordinates.py

Get gps coordinates from address.

Uses geopy [Nomatim](https://geopy.readthedocs.io/en/stable/#nominatim) geocoder.

Example of use :
```bash
$ ./get_gps_coordinates.py "1 place de la concorde 75008 Paris" -v
Place de la Concorde, Quartier des Champs-Élysées, Paris 8e Arrondissement, Paris, Île-de-France, France métropolitaine, 75008, France
48.8660227,2.3198494
```
