#!/usr/bin/env python3

import argparse
from geopy.geocoders import Nominatim
import logging
from custom_logging import CustomFormatter


logger = logging.getLogger(__name__)
logging_level = logging.INFO

def get_gps_coordinates(address):
    geolocator = Nominatim(user_agent="script/python/get_gps_coordinates")
    location = geolocator.geocode(address)
    return location

def main():

    parser = argparse.ArgumentParser(description='Get GPS coordinates from address')
    parser.add_argument('--verbose', '-v', action="store_true" , help='verbose mode')
    parser.add_argument('address',                               help='address to geocode')
    args = parser.parse_args()
    
    # logging level
    logging_level = logging.DEBUG if args.verbose else logging.INFO
    
    # logging to console
    logger.setLevel(logging_level)
    stdout_handler = logging.StreamHandler()
    stdout_handler.setFormatter(CustomFormatter())
    logger.addHandler(stdout_handler)
    location = get_gps_coordinates(args.address)
    
    logger.debug(location.address)
    print(f"{location.latitude},{location.longitude}")

if __name__ == "__main__":
    main()
    
