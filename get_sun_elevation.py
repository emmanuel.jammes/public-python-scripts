#!/usr/bin/env python3

import argparse
import ephem
from datetime import datetime, timedelta
from dateutil import parser
import math
from geopy.geocoders import Nominatim
from termcolor import colored
import logging
from custom_logging import CustomFormatter

OUTPUT_DATE_FORMAT = "%Y-%m-%d"

logger = logging.getLogger(__name__)

def get_gps_coordinates(address):
    geolocator = Nominatim(user_agent="script/python/get_sun_elevation")
    location = geolocator.geocode(address)
    return location

def get_sun_elevation(lat, lon, observation_date):
    o = ephem.Observer()
    o.lat = str(lat)
    o.lon = str(lon)
    o.horizon = '-0:34'
    o.date = observation_date
    sun = ephem.Sun()
    sun.compute(o)

    sun_alt = ephem.degrees(sun.alt)
    sun_elevation = "{:.2f}".format(float(math.degrees(sun_alt)))
    return sun_elevation


def argparse_gps_coords(s):
    try:
        lat, lon = map(str, s.split(','))
        return lat, lon
    except:
        raise argparse.ArgumentTypeError("Coordinates must be lat, lon")

def argparse_date(s):
    try:
        return parser.parse(s)
    except ValueError:
        msg = "not a valid date: {0!r}".format(s)
        raise argparse.ArgumentTypeError(msg)

def main():

    # parse args
    parser = argparse.ArgumentParser(description='Calculate max sun elevation at location')
    parser.add_argument('--verbose', '-v', action="store_true" ,                    help='verbose mode')
    group = parser.add_mutually_exclusive_group(required=True)
    group.add_argument('--coords',  '-c', type=argparse_gps_coords,                 help='gps coordinates of location')
    group.add_argument('--address', '-a',                                           help='address of location')
    parser.add_argument('--date',    '-d', type=argparse_date,       required=True, help='start date for calculation')
    parser.add_argument('--nbdays',  '-n', type=int,                 default=1,     help='nb of days for calculation (default: %(default)s)')
    args = parser.parse_args()

    # logging level
    logging_level = logging.DEBUG if args.verbose else logging.INFO
    
    # logging to console
    logger.setLevel(logging_level)
    stdout_handler = logging.StreamHandler()
    stdout_handler.setFormatter(CustomFormatter())
    logger.addHandler(stdout_handler)

    start_date = args.date
    nb_days = args.nbdays

    # if address given
    if args.address:

        # call geocoding api
        location = get_gps_coordinates(args.address)
        lat = location.latitude
        lon = location.longitude

        logger.debug(f"address found: {location.address}")

    # if gps coordinates given
    elif args.coords :
        lat, lon = args.coords

    logger.debug(f"gps coords: {lat},{lon}")
    
    for day in range(nb_days):
        observation_date = start_date + timedelta(hours=12) + timedelta(days = day)
        print("%s: %s" % (observation_date, get_sun_elevation(lat, lon, observation_date)))

if __name__ == "__main__":
    main()
